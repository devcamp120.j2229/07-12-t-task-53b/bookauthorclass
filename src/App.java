import com.devcamp.book.Author;
import com.devcamp.book.Book;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Author author1 = new Author("Le Van Nam", "namlv@gmail.com", 'm');
        System.out.println(author1.toString());

        Author author2 = new Author("Nguyen Hien", "hiennguyen@gmail.com", 'f');
        System.out.println(author2.toString());

        Book book1 = new Book("Java", author1, 30000);
        System.out.println(book1);

        Book book2 = new Book("Nodejs", author2, 50000, 5);
        System.out.println(book2);
    }
}
