package com.devcamp.book;

public class Author {
    private String name;
    private String email;
    private char gender = 'f';
    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getName() {
        return name;
    }
    public char getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return "Author [name=" + this.name + ",email" + this.email + ",gender=" + this.gender + "]";
    }
}
